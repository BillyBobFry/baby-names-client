export type Gender = "boy" | "girl";

export type Name = {
  name: string;
  gender: Gender;
  meaning?: string | null;
  origin?: string | null;
};

export type DataPoint = {
  count: number;
  year: number;
  rank: number;
};

export type NameData = Name & {
  data: DataPoint[];
};

export type HSL = {
  hue: number;
  saturation: number;
  light: number;
};
