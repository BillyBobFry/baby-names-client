import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

const theme = {
  themes: {
    light: {
      primary: "#03A9F4",
      secondary: "#757575",
      accent: "#FFC107",
      error: "#b71c1c",
    },
  },
  options: {
    customProperties: true,
  },
};

export default new Vuetify({ theme });
