import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import NameSearch from "../components/NameSearch.vue";
import Kindling from "../components/Kindling.vue";
import Popularity from "../components/Popularity.vue";

Vue.use(VueRouter);

const kindlingRoutes: Array<RouteConfig> = [
  {
    path: "/kindling",
    name: "Name Kindling",
    component: Kindling,
  },
  {
    path: "/kindling/:id",
    name: "Name Kindling id",
    component: Kindling,
  },
  {
    path: "/kindling/:id/gender/:gender",
    name: "Name Kindling id-gender",
    component: Kindling,
  },
  {
    path: "/kindling/:id/gender/:gender/:mode?",
    name: "Name Kindling id-gender-mode",
    component: Kindling,
  },
  {
    path: "/kindling/:id/matches/:otherID",
    name: "Name Kindling id-matches",
    component: Kindling,
  },
  {
    path: "/kindling/matches/:otherID",
    name: "Name Kindling matches-otherID",
    component: Kindling,
  },
  {
    path: "/kindling/:id/:mode",
    name: "Name Kindling id-mode",
    component: Kindling,
  },
];

export const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Search",
    component: NameSearch,
  },
  {
    path: "/ranking",
    name: "Ranking",
    component: NameSearch,
  },
  {
    path: "/popularity",
    name: "Popularity",
    component: Popularity,
  },
  ...kindlingRoutes,
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
