export const paletteGenerator = (
  baseColour: { hue: number; saturation: number; light: number },
  index: number
): string => {
  const { hue, saturation, light } = baseColour;
  return `hsl(${(hue + index * 139) % 360}, ${saturation}%, ${light}%)`;
};

// factors of 360:
// 180, 120, 90, 72, 60, 45, 40, 36, 33,
